package exam4;

import java.util.Set;

public interface SJISVariation {
    Set<Character> findUnMatchChars();
}
