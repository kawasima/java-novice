package exam6;

public interface BranchNameValidator {
    boolean validate(String name);
}
