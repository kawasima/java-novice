package exam3;

import java.util.Set;

public interface DifferenceDetector {
    Diff detect(Set<User> pre, Set<User> post);
}
