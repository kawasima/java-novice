package exam2;

import net.unit8.kysymys.scorer.KysymysTestLauncher;
import net.unit8.kysymys.scorer.SutDetector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CardNumberValidatorTest {
    CardNumberValidator sut;

    @BeforeEach
    void setup() {
        sut = SutDetector.detect(CardNumberValidator.class);
    }

    @Test
    void amex() {
        assertThat(sut.isValid("49927398716")).isTrue();
    }

    @Test
    void diners() {
        assertThat(sut.isValid("30569309025904")).isTrue();
    }
    @Test
    void jcb() {
        assertThat(sut.isValid("3530111333300000")).isTrue();
    }
    @Test
    void master() {
        assertThat(sut.isValid("5555555555554444")).isTrue();
    }
    @Test
    void visa() {
        assertThat(sut.isValid("4111111111111111")).isTrue();
    }

    @Test
    void invalid() {
        assertThat(sut.isValid("1111111111111111")).isFalse();
    }

    public static void main(String[] args) {
        KysymysTestLauncher.run(CardNumberValidatorTest.class);
    }
}