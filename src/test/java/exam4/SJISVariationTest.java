package exam4;

import net.unit8.kysymys.scorer.KysymysTestLauncher;
import net.unit8.kysymys.scorer.SutDetector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SJISVariationTest {
    SJISVariation sut;

    @BeforeEach
    void setup() {
        sut = SutDetector.detect(SJISVariation.class);
    }

    @Test
    void containsUnMatchChars() {
        assertThat(sut.findUnMatchChars()).containsExactlyInAnyOrder('―', '～', '∥', '－', '￤');
    }

    public static void main(String[] args) {
        KysymysTestLauncher.run(SJISVariationTest.class);
    }
}