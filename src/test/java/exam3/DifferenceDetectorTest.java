package exam3;

import net.unit8.kysymys.scorer.KysymysTestLauncher;
import net.unit8.kysymys.scorer.SutDetector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class DifferenceDetectorTest {
    DifferenceDetector sut;

    @BeforeEach
    void setup() {
        sut = SutDetector.detect(DifferenceDetector.class);
    }

    @Test
    void diff() {
        Set<User> pre = Set.of(
                new User("クロノジェネシス", 1),
                new User("キセキ", 2),
                new User("モズベッロ", 3),
                new User("サートゥルナーリア", 4),
                new User("メイショウテンゲン", 5),
                new User("ラッキーライラック", 6),
                new User("トーセンスーリヤ", 7),
                new User("トーセンカンビーナ", 8),
                new User("ダンビュライト", 9),
                new User("レッドジェニアル", 10),
                new User("スティッフェリオ", 11),
                new User("カデナ", 12),
                new User("ワグネリアン", 13),
                new User("アドマイヤアルバ", 14),
                new User("ペルシアンナイト", 15),
                new User("ブラストワンピース", 16),
                new User("グローリーヴェイズ", 17),
                new User("アフリカンゴールド", 18)
        );
        Set<User> post = Set.of(
                new User("クロノジェネシス", 1),
                new User("ユニコーンライオン", 2),
                new User("レイパパレ", 3),
                new User("カレンブーケドール", 4),
                new User("キセキ", 5),
                new User("ミスマンマミーア", 6),
                new User("カデナ", 7),
                new User("モズベッロ", 8),
                new User("アリストテレス", 9),
                new User("ワイプティアーズ", 10),
                new User("メロディーレーン", 11),
                new User("アドマイヤアルバ", 12),
                new User("アフリカンゴールド", 13)
        );

        Diff diff = sut.detect(pre, post);
        assertThat(diff.getAddedUsers().stream().map(User::getName)).allMatch(name -> Set.of(
                        "アリストテレス", "ワイプティアーズ", "カレンブーケドール", "メロディーレーン", "ユニコーンライオン", "レイパパレ", "ミスマンマミーア")
                .contains(name));
        assertThat(diff.getModifiedUsers().stream().map(User::getName)).allMatch(name -> Set.of(
                        "キセキ", "モズベッロ", "アフリカンゴールド", "カデナ", "アドマイヤアルバ")
                .contains(name));
        assertThat(diff.getRemovedUsers().stream().map(User::getName)).allMatch(name -> Set.of(
                        "グローリーヴェイズ",
                        "ラッキーライラック",
                        "メイショウテンゲン",
                        "ペルシアンナイト",
                        "トーセンカンビーナ",
                        "スティッフェリオ",
                        "ワグネリアン",
                        "レッドジェニアル",
                        "ブラストワンピース",
                        "ダンビュライト",
                        "トーセンスーリヤ",
                        "サートゥルナーリア")
                .contains(name));
    }

    public static void main(String[] args) {
        KysymysTestLauncher.run(DifferenceDetectorTest.class);
    }
}