package exam5;

import net.unit8.kysymys.scorer.KysymysTestLauncher;
import net.unit8.kysymys.scorer.SutDetector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AnchorURLTest {
    AnchorURL sut;

    @BeforeEach
    void setup() {
        sut = SutDetector.detect(AnchorURL.class);
    }

    @Test
    void replaceUrl() {
        String input = sut.convert("あいうwww.google.com/search?q=a&b=cえお");
        assertThat(sut.convert(input)).isEqualTo("あいう<a href=\"www.google.com/search?q=a&b=c\">www.google.com/search?q=a&b=c</a>えお");
    }

    @Test
    void inComment() {
        String input = sut.convert("あいう<!-- www.google.com/search?q=abc -->えお");
        assertThat(sut.convert(input)).isEqualTo(input);
    }

    @Test
    void anchor() {
        String input = "あいう<a href='www.google.com/search?q=abc'>google</a>えお";
        assertThat(sut.convert(input)).isEqualTo(input);
    }

    public static void main(String[] args) {
        KysymysTestLauncher.run(AnchorURLTest.class);
    }
}